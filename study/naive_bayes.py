from __future__ import division
import numpy as np 
import pandas as pd
import math

def Bayesian_learner(training_set):
    mean=training_set.groupby('class').aggregate(np.mean)
    variance=training_set.groupby('class').aggregate(np.var)
    count_true=0
    count_false=0
    
    for iclass in training_set.iloc[:,-1]:        
        if iclass=='true':
            count_true+=1
        else:
            count_false+=1    
    total=training_set.iloc[:,-1].count()
    Probability_true=count_true/total
    Probability_false=count_false/total    
    return (mean, variance, Probability_true, Probability_false)

def Bayesian_classifier(Bayes_statistics, test):
    mean, variance, probability_true, probability_false=Bayes_statistics
    probability_category_true=1
    probability_category_false=1
    prediction=[]
    count=0
    for h in test.index:
        example=test.ix[h]
        for i in range(example.count()):
            probability_category_true*=(1/math.sqrt((2*math.pi*variance.ix['true',i])))*math.exp((-((example.iloc[:,i]-mean.ix['true',i])**2))/(2*variance.ix['true',i]))
            probability_category_false*=(1/math.sqrt((2*math.pi*variance.ix['false',i])))*math.exp((-((example.iloc[:,i]-mean.ix['false',i])**2))/(2*variance.ix['false',i]))  
        probability_category_true*=probability_true
        probability_category_false*=probability_false    
        if probability_category_true>probability_category_false:
            #return True
            prediction.append(1)
        else:
            prediction.append(0)
            #return False
    return prediction
