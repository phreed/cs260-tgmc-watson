# coding: latin-1
#
# Team members contributing to the content of this file:
#   Fred Eisele 
#   Lina Sulieman 
#   
#   (Doug Kirkpatrick : correlation_statistics function )

import numbers
import pandas as pd
import numpy as np
from sampling import sampling
from tgmc import *

# see Decision-Tree notebook for usage

def classifier( decision_tree, question):
    '''
    performs a walk of the tree to a leaf,
    reporting the content of the leaf.
    '''
    isLeaf = decision_tree[0]
    if isLeaf:
        # print "is leaf: ", isLeaf, " tree: ", decision_tree
        (_, class_count_dict) = decision_tree
        return class_count_dict
    (_, selector, children) = decision_tree
    (fn,txt) = selector
    choosen_subtree = children[fn(question)]
    # print "choice: ", choice
    return classifier( choosen_subtree, question )
    
def winner_classifier( decision_tree, question ):
    '''
    obtains a leaf using the general classifier 
    and determines a winner for the classification 
    having the greatest count.
    '''
    classification = classifier( decision_tree, question )
    winner = ''
    maximum = 0
    for (key, value) in classification.iteritems():
        if key == 'total': continue
        if value < maximum: continue
        winner = key
        maximum = value
    return winner

'''    
# Function    Description
# count    Number of non-null observations
# sum    Sum of values
# mean    Mean of values
# mad    Mean absolute deviation
# median    Arithmetic median of values
# min    Minimum
# max    Maximum
# abs    Absolute Value
# prod    Product of values
# std    Unbiased standard deviation
# var    Unbiased variance
# skew    Unbiased skewness (3rd moment)
# kurt    Unbiased kurtosis (4th moment)
# quantile    Sample quantile (value at %)
# cumsum    Cumulative sum
# cumprod    Cumulative product
# cummax    Cumulative maximum
# cummin    Cumulative minimum
'''
    
def adjuster(frame, groups, functor_name, functor):
    '''
    computes a particular statistic for the group
    partition as well as the whole.    
    '''
    df = groups.aggregate(functor)
    df = df.reset_index()
    df['level'] = 'part'
    totals = pd.DataFrame(frame.apply(functor)).T
    totals['level'] = 'whole'
    totals['class'] = 'all'
    result = df.append(totals)
    result['statistic'] = functor_name
    return result.set_index(['statistic','level','class'])
        
def simple_statistics( training_frame ):
    '''
    calculates the statistics which will be used 
    by the simple-terminator and simple-elector functions.
    '''
    if training_frame.empty:
        return None
    total = training_frame
    groups = training_frame.groupby('class')
    counter = lambda x : len(x)
    countDf = adjuster(total, groups, 'count', counter)
    meanDf = adjuster(total, groups, 'mean', np.mean)
    stdevDf = adjuster(total, groups, 'stdev', np.std)
    rangeDf = adjuster(total, groups, 'range', lambda x : np.max(x) - np.min(x))
    return pd.concat([countDf, rangeDf, stdevDf, meanDf])
    
def correlation_statistics( training_frame ):
    '''
    Based on Pearson's correlation
    an alternate set of statistics to be used by 
    the correlation-elector
    '''
    total = training_frame
    groups = training_frame.groupby('class')
    counter = lambda x : len(x)
    countDf = adjuster(total, groups, 'count', counter)
    meanDf = adjuster(total, groups, 'mean', np.mean)
    stdevDf = adjuster(total, groups, 'stdev', np.std)
    rangeDf = adjuster(total, groups, 'range', lambda x : np.max(x) - np.min(x))
    return pd.concat([countDf, rangeDf, stdevDf, meanDf])
    
def entropy_statistics( training_frame ):
    '''
    this was intended to compute the entropy statistic
    for the features.  
    '''
    None
    
def simple_terminator( statistics, training_frame, threshold=9):
    '''
    true indicates termination.
    This function acts as a safety net.
    It determines if more features should be examined.
    '''
    if statistics is None:
        return True
    if training_frame.empty:
        return True
    
    counts_whole = statistics.xs(('count','whole'), level=('statistic','level'))
    total = counts_whole.at['all',0]
    if total < threshold:
        # print "insufficient"
        return True
    # print "terminator problem: \n", statistics
    counts_parts = statistics.xs(('count','part'), level=('statistic','level'))
    return (counts_parts[0] < 2).any()
    
def leaf_maker(statistics, training_frame):
    '''
    makes the leaves in the decision tree.
    e.g. (True, {'c1': 20, 'c2': 2})
    The 'True' indicates that this is a leaf node.
    The 'c#' indicates the classification value and 
    the number indicates the number of occurances in 
    the training data leading to this leaf.
    '''
    if statistics is None:
        return (True, {})
    if training_frame.empty:
        return (True, {})
    counts_parts = statistics.xs(('count','part'), level=('statistic','level'))
    return (True, counts_parts.loc[:,0].to_dict())
    
def selector_maker(level, learner, elected_feature, threshold, training_frame, active_features):
    '''
    makes selection nodes for the decision tree.
    e.g. (False, (lambda values : 0 if values[0] < 5.0 else 1, 'v_0 @ 5.0'), [<subtree array>])
    The 'False' indicates that this node is not a leaf (a selection node).
    The lambda function returns the index into the 'subtree array'.
    The text is provided as a summary of the lambda function;
    the text is useful in printing the decision tree in a form
    which is human understandable.
    '''
    criterion = training_frame.loc[:,elected_feature].map(lambda value : value < threshold )
    selector_txt = "v_{0} @ {1}".format(elected_feature, threshold)
    # print "selector: ", selector_txt
    lhs = training_frame.loc[criterion]
    # print "lhs: ", lhs
    lh_tree = learner(level, lhs, active_features)
    rhs = training_frame.loc[~criterion]
    # print "rhs: ", rhs
    rh_tree = learner(level, rhs, active_features)
    selector_fn = lambda value : 0 if value[elected_feature] < threshold else 1
    return (False, (selector_fn,selector_txt), [lh_tree, rh_tree])
    
def trivial_elector(level, learner, statistics, training_frame, active_features, class_type):
    '''
    electors build the decision tree nodes.
    This trivial elector causes the decision tree to 
    provide the trivial case of guessing the classification
    without regard for any features.  It only cares about
    the frequency of the classifications in the training data.
    '''
    return leaf_maker(statistics, training_frame)
    
def random_elector(level, learner, statistics, training_frame, active_features, class_type):
    '''
    An unsupervised learner; it doesn't use the classification values.
    It selects a feature arbitrarily and splits the data on it.
    '''
    range_set = statistics.xs('range', level='statistic')
    # print "range set:\n", range_set, '\n active features: \n', active_features
    reduced_features = [ix for ix in active_features if range_set.loc['total',ix] > 0.001]
    if not reduced_features:
        return leaf_maker(statistics, training_frame)
    
    elected_feature = reduced_features.pop()
    meanSet = statistics.xs('mean', level='statistic')
    mean = meanSet.at[True,elected_feature]
    # print "election: ", elected_feature, ' mean: ', mean
    
    # print "predicate: ", selector_predicate(training_frame)
    return selector_maker(level, learner, elected_feature, 
                          mean, training_frame, reduced_features) 
    

def simple_boolean_elector(level, learner, statistics, training_frame, active_features, class_type):
    '''
    This elector looks for a large difference in the means (normalized by the standard-deviation).
    m1, m0 : means for class 1 (True) and 0 (False)
    s1, s0 : standard-deviations for class 1 (True) and 0 (False)
    The winning feature is found by maximizing 
      maximize |m1-m0|/(s1+s0)
    The cut point for the winning feature is between the two means
    cut = (m1*s0 + m0*s1)/(s1+s0)
    '''
    range_set_whole = statistics.xs(('range','whole'), level=('statistic','level'))
    # print "range set:\n", range_set, '\n active features: \n', active_features
    reduced_features = [ix for ix in active_features if range_set_whole.at['all',ix] > 0.001]
    if not reduced_features:
        return leaf_maker(statistics, training_frame)
    
    mean_part = statistics.xs(('mean','part'), level=('statistic','level'))
    stdev_part = statistics.xs(('stdev','part'), level=('statistic','level'))

    (v0, v1) = (class_type[0],class_type[1])
    try:
        m0 = mean_part.loc[v0, :]
        m1 = mean_part.loc[v1, :]
        s0 = stdev_part.loc[v0, :] + 1.0
        s1 = stdev_part.loc[v1, :] + 1.0
    except KeyError:
        return leaf_maker(statistics, training_frame)    
    score = abs((m0 - m1)/(s0 + s1))
    # print "statistics: mean:\n", mean_part, "\n stdev:\n", stdev_part, "\n m0:\n", m0, "\n s0:\n", s0, "\n s1:\n", s1, "\n score:\n", score
    ef = score.argmax() # elected_feature_index
    threshold = (m1[ef]*s0[ef] + m0[ef]*s1[ef])/(s1[ef]+s0[ef])
    new_reduced_features = [x for x in reduced_features if x != ef]
    return selector_maker(level, learner, ef, 
                          threshold, training_frame, new_reduced_features) 

def learner( statistics_function, elector, terminator, training_frame, maxLevels=8):
    '''
    The learner builds a decision tree based on information found
    in the training_frame.  The training_frame is expected to be a 
    pandas data frame with the features having integer column identifiers
    and the classification column being named 'class'.
    The statistics_function, elector, and terminator are expected to
    be a matched set.  The elector creates subtrees and the terminator
    is predicate which indicates that a branch in the tree is
    not worth further expansion.
    
    This learner has only been tested with two class types (True, False).
    '''
    class_type = training_frame['class'].unique()
    
    def helper(level, reduced_frame, active_features):        
        #print "level: ", level
        if level > maxLevels:
            return (True, {})
        # print "active features: ", len(active_features)
        statistics = statistics_function(reduced_frame)
        if terminator( statistics, reduced_frame ):
            leaf = leaf_maker(statistics, reduced_frame)
            # print "terminating: ", leaf 
            return leaf
        branch = elector(level+1, helper, statistics, reduced_frame, active_features, class_type)
        # print "branch: ", branch 
        return branch
    
    active_features = [ col for col in training_frame.columns if isinstance(col, numbers.Number)]
    return helper(0, training_frame, active_features)
    
def DTForest_learner(trainSet,treesNum=5,method="Bagging",SetPick=0.7):
    '''
    The function expect the training set, the number of trees to build, the method to sample
    until  now we only have bagging, and the precetage of the training data that we will sample from.
    We will have a loop of Ntrees, we will build Ntrees and each tree will be appended in the ForestList variable to 
    return them and use them in the classifier.
    For each tree, we call the sampling function that will randomly pick part of the data and save in in sampling dataframe.
    The sampling dataframe will be sent to the DT learner athat will apply some statistical methods to learn the features to 
    split on and we will get back the learnt tree.
    at the end of the function we will have a list that stores all the trees we built and learnt and return this list to the main function.
    '''
    ForestList=[]
    for i in range (0,treesNum):

        trainSetRowID=trainSet.iloc[:,0]
        trainSetRowID=range(0,len(trainSetRowID))
        colNumber=len(trainSet.iloc[0,:])
        Features=range(0,(colNumber-1))
        sampleSet=sampling(trainSetRowID,method)
        sampleDataFrame=trainSet.iloc[sampleSet,:]
        simple_decision_tree = learner( simple_statistics, simple_boolean_elector, simple_terminator, sampleDataFrame)
        ForestList.append(simple_decision_tree)
    return ForestList

def DTForestclassifier (learntForest,holdBackSet,treesNum):
    '''
    the classifier will use the learnt forests or use the information from all the learnt tree, the saved fold or the hold set fold 
    that will apply the classification on, and the number of trees.
    For each hold set row or item, we are going to classify or predict the class of the row using all the trees we learnt.
    We will loop through our forest, and get each tree, send the tree and the row we want to classify.
    We will count the number of times that this row was labled as TRUE, in this way we will have the number of times
    that this row was predicted as true.  
    treesDTHoldSetPrediction will have the number of time we labels the corresponding row as true.
    Once we predicted the calss of row_i using all the tree, we will calculate the probability that this row is predicted as true
    by dividing the count we got from the first steps ( true counts) over the number of all trees. Hence we will have a probability of labelling that row as True.
    The returned list will be the probabilty of true label for each item in the hold set.
    '''
    treesDTHoldSetPrediction=[]
    treesDTHoldSetPrediction=[0]*len(holdBackSet.index)
    count=0
    forestClassification=[0]*len(holdBackSet.index)
    #print("Forest",len(learntForest))
    for i in holdBackSet.index:
        
        for tree in learntForest:
            
            
            baggingPredictedClass=winner_classifier(tree,holdBackSet.ix[i])
            #print(baggingPredictedClass)
            if baggingPredictedClass==True:
                
                treesDTHoldSetPrediction[count]=treesDTHoldSetPrediction[count]+1
                #print(treesDTHoldSetPrediction[count])
        count=count+1
        # the set that will be sent to the classifier is holdBackSet
        # Compare the aggregrated classifier results with the correct class or true class
        # the DTprediction results holds the number of predictions that were TRUE
        # We need to divide on the number of tree to get a probabilty value that class=True for a specific row
    treesDTHoldSetTrueProb= map(lambda x: (x*1.0) / (treesNum*1.0), treesDTHoldSetPrediction)
    
    return treesDTHoldSetTrueProb
