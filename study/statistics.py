
# Doug's stats functions
# Edited by Doug
 
def biserrial_correlation_coefficient_elector(learner, statistics, training_frame, active_features):
    range_set = statistics.xs('range', level='statistic')
    # print "range set:\n", range_set, '\n active features: \n', active_features
    reduced_features = [ix for ix in active_features if range_set.loc['total',ix] > 0.001]
    if not reduced_features:
        return leaf_maker(statistics, training_frame)
    
    mean_set = statistics.xs('mean', level='statistic')
    stddev_set = statistics.xs('stddev', level='statistic')
	counter_set = statistics.xs('count', level='statistic')
    elected_feature = -1
    score = 0.0
    threshold = 0.0
    for fx in statistics:
        m1 = mean_set.at[True, fx]
        m0 = mean_set.at[False, fx]
		sN = stddev_set.at['whole', fx]
		c1 = counter_set.at[True, fx]
		c0 = counter_set.at[False, fx]
        coeff = (m1-m0)/sN * ((c1*c0/((c1+c0)**2))**1/2)
        if c_score > score: 
            continue
        elected_feature = fx
        score = c_score
        threshold = (m1*s0 + m0*s1)/(s1+s0)
    # print score, threshold
    new_reduced_features = [x for x in reduced_features if x != elected_feature]
    return selector_maker(level, learner, elected_feature, 
                          threshold, training_frame, new_reduced_features) 
						  
def entropy_elector(learner, statistics, training_frame, active_features):
    range_set = statistics.xs('range', level='statistic')
    # print "range set:\n", range_set, '\n active features: \n', active_features
    reduced_features = [ix for ix in active_features if range_set.loc['total',ix] > 0.001]
    if not reduced_features:
        return leaf_maker(statistics, training_frame)
    
    mean_set = statistics.xs('mean', level='statistic')
    stddev_set = statistics.xs('stddev', level='statistic')
    elected_feature = -1
    score = 0.0
    threshold = 0.0
    for fx in statistics:
        m1 = mean_set.at[True, fx]
        m0 = mean_set.at[False, fx]
        s1 = stdev_set.at[True, fx]
        s0 = stdev_set.at[False, fx]
        c_score = ((m1 - m0)/(s1 - s0))**2
        if c_score < score: 
            continue
        elected_feature = fx
        score = c_score
        threshold = (m1*s0 + m0*s1)/(s1+s0)
    # print score, threshold
    new_reduced_features = [x for x in reduced_features if x != elected_feature]
    return selector_maker(level, learner, elected_feature, 
                          threshold, training_frame, new_reduced_features) 