# coding: latin-1

import csv
import os
import numpy as np
import pandas as pd
import tempfile


def load_types(csvfilepath, number_of_types_to_load=1):
    with open(csvfilepath, 'rb') as csvfile:
        tdreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        type_id = 0
        type_count = 0
        train = {}
        question = []
        for candidate in tdreader:
            if type_id != candidate[1]:
                type_count += 1
                if type_count > number_of_types_to_load: 
                   break
                type_id = candidate[1]
                question = []
                train[type_id] = question
                
            candidate_id = int(candidate[0])
            features = map(float, candidate[2:-2])
            is_correct = candidate[-1] in ['true', 'True', 't', 'y', 'yes', 'Y']
            
            question.append( {'id': candidate_id, 'class': is_correct, 'features': features} )

    print "questions: ", [ key + ':' + str(len(value)) for (key,value) in train.iteritems() ]
    return train
	
def load_statements(csv_file_path, load_type_count=1):
    with open(csv_file_path, 'rb') as csvfile:
        tdreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        type_id = 0
        type_count = 0
        candidates = []
        for candidate in tdreader:
            if type_id != candidate[1]:
                type_count += 1
                if type_count > load_type_count: 
                   break
                type_id = candidate[1]
                
            candidate_id = int(candidate[0])
            features = map(float, candidate[2:-2])
            is_correct = candidate[-1] in ['true', 'True', 't', 'y', 'yes', 'Y']
            
            candidates.append( {'id': candidate_id, 'class': is_correct, 'features': features} )
    return candidates

def load_array(csv_file_path):
    d1=np.genfromtxt(csv_file_path, delimiter=',')
    
    d1=np.array([row for row in csv.reader(open(csv_file_path))]) 
    #d1=np.loadtxt(csv_file_path, delimiter = ',')
    
    
    with open(csv_file_path, 'r') as f:
        data = []
        for line in f:
            #print(line)
            line = line.strip().split(',')
            #print(line[0:-1])
            numpart=map(float, line[0:-1])
            #print(line[-1])
            boolPart=''.join(map(str, line[-1]))
            
            if boolPart=='FALSE':
                boolPart=False
                
                numpart.append(0.0)
            else:
                boolPart=True
                numpart.append(1.0)
            #print (numpart)
            data.append(numpart)
            
    return np.array(data)
	
def load_frame(csv_file_path):
    data = load_array(csv_file_path)
    print "data shape: ", data.shape
    #print data[0]
    ids = data[:,0]
    #print(data[0:5,:])
    #data[:,2:-2]=map(float,data[:,2:-2])
    
    frame = pd.DataFrame(data[:,2:-1], index=ids, dtype=float)
    #print(data[0:5,:])
    cList=[]
    for row in range(len(data[:,0])):
        if data[row,-1] ==0.0:
            data[row,-1]=False
            cList.append(False)
        else:
            data[row,-1]=True
            cList.append(True)
    frame['class'] = pd.DataFrame(cList, index=ids)
    
    
    #print(frame['class'])
    return frame
