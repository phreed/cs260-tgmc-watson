# -*- coding: cp1252 -*-
import math
from random import randrange
import random 


# candidatesVec: a vector of rows id of training set
# method: Bagging or RF, to use RF just put in the method variable any value other than bagging
# trainingPerc: the percentage of training set that we need to pick to construct the training set. e.g for 0.7 we will pick 70% of training set to train our model on
# features: for Rf, we need to randoly pick the features, for bagging we don't need to  that's why I assigned a default value as All
def sampling(candidatesVec,method="Bagging",withReplacement=False,trainingPerc=0.7,FeaturesVec="ALL"):
	# vector stroing the rows ID that will be used in model training
	trainingCanList=[]
	# Took a copy to make sure we are not manipualting the original vector
	candidatesVecCopy=[]
	for i in range(0,len(candidatesVec)):
	    candidatesVecCopy.append(candidatesVec[i])
	    
	
	#candidatesVecCopy=candidatesVec.copy()
	tariningItemsNum=math.floor(len(candidatesVec)*0.7)
	# form the list od Candidates ID that will be used in , Necessary for both bagging and RF
	print(tariningItemsNum)
	for pick in range(0,int(tariningItemsNum)):
		value=random.choice(candidatesVecCopy)
		trainingCanList.append(value)
		if withReplacement ==False:
      		    candidatesVecCopy.remove(value)
	
	if method=="Bagging":
		return trainingCanList
	# if not bagging, then we will do random forest
	
	# pick the number of features you will pick randomly
	# some resources recomended to select randomely a number of features equals to sqrt of features
	featuresTry=int(math.floor(math.sqrt(len(FeaturesVec))))
	featuresTry=int(len(FeaturesVec)/2)
	featuresList=[]
	
	featuresCopy=[]
	for i in range (0,int(len(FeaturesVec))):
	   featuresCopy.append(FeaturesVec[i]) 
	for fpick in range(0,featuresTry):
		featureVal=random.choice(featuresCopy)
		featuresList.append(featureVal)
		featuresCopy.remove(featureVal)
	
	#Forming the Dictinary or hash that have features and sampling vectors, this value will be returned to be used to train model
	RFSample={'features':FeaturesVec,'rows':trainingCanList}
	return RFSample
	
	
	
s=[]
for i in range(0,20):
    s.append(i)
f=['F1','F2','F3','F4']
#print (sampling(s,"RF",FeaturesVec=f))
 